%% Description
% Soleno matlab edition rewrite by Jeroen van Nugteren
% Soleno was originally developed at Twente University by Gert Mulder, and upgraded in 1999 by Erik Krooshoop

% calculation speed:
% enabling parallel computing with 'matlabpool #' increases speed by a great deal
% speed may be increased further by using the mex files

%% Main Control function
function [Brt,Bzt] = soleno_calcB2(R, Z, Rin, Rout, Zlow, Zhigh, I, Nlayers)

% check input sizes
if size(R,1)~=size(Z,1) || size(R,2)~=size(Z,2); error('The sizes of R and Z must be equal.'); end;
    
% reshape input
[a,b] = size(R);
R = reshape(R,[],1);
Z = reshape(Z,[],1);

% allocate
Brt = zeros(1,length(R));
Bzt = zeros(1,length(Z));

% walk over target positions
for j = 1:length(Rin)
    parfor i = 1:length(R)
        [Brtt,Bztt] = Sol_CalcB_new(R(i), Z(i), Rin(j), Rout(j), Zlow(j), Zhigh(j), I(j), Nlayers(j));
        Brt(i) = Brt(i) + Brtt;
        Bzt(i) = Bzt(i) + Bztt;
    end
end

% reshape output
Brt = reshape(Brt,a,b);
Bzt = reshape(Bzt,a,b);

end

%% Improved version of Soleno
% better field in the radial direction
function [Brt,Bzt] = Sol_CalcB_new(R, Z, Rin, Rout, Zlow, Zhigh, I, Nlayers)
% set layers if not set remotely 5 is default value
if isempty(Nlayers); Nlayers = 5;
elseif Nlayers < 1; Nlayers = 5; end 

if R >= Rin && R <= Rout
    % internal
    A = (R - Rin) / (Rout - Rin);
    NL1 = floor(Nlayers * A) + 1;
    NL2 = floor(Nlayers * (1 - A)) + 1;
    % deel1
    [Brt,Bzt] = Sol_CalcB(R, Z, Rin, R, Zlow, Zhigh, I * A, NL1);
    Br2 = Brt;
    Bz2 = Bzt;
    [Brt,Bzt] = Sol_CalcB(R, Z, Rin, R, Zlow, Zhigh, I * A, NL1 * 2);
    BZtemp = Bzt + (Bzt - Bz2) / 3; %Extrapolatie naar 1/Nl/Nl=0
    BRtemp = Brt + (Brt - Br2) / 3;
    % deel2
    [Brt,Bzt] = Sol_CalcB(R, Z, R, Rout, Zlow, Zhigh, I * (1 - A), NL2);
    Bz2 = Bzt;
    Br2 = Brt;
    [Brt,Bzt] = Sol_CalcB(R, Z, R, Rout, Zlow, Zhigh, I * (1 - A), NL2 * 2);
    Bzt = BZtemp + Bzt + (Bzt - Bz2) / 3; %Extrapolatie naar 1/Nl/Nl=0
    Brt = BRtemp + Brt + (Brt - Br2) / 3;
else
    % external
    [Brt,Bzt] = Sol_CalcB(R, Z, Rin, Rout, Zlow, Zhigh, I, Nlayers);
    Bz2 = Bzt;
    Br2 = Brt;
    [Brt,Bzt] = Sol_CalcB(R, Z, Rin, Rout, Zlow, Zhigh, I, Nlayers * 2);
    Bzt = Bzt + (Bzt - Bz2) / 3; %Extrapolatie naar 1/Nl/Nl=0
    Brt = Brt + (Brt - Br2) / 3;
end

end

%% Original Soleno Function (used by improved version)
function [Brt,Bzt] = Sol_CalcB(R, Z, Rin, Rout, Zlow, Zhigh, It, Nlayers)
Brd = 0;
Bzd = 0;

if Rin ~= Rout && Zlow ~= Zhigh
    Factor = It / (Zhigh - Zlow) / Nlayers;
    if Factor ~= 0
        Zm1 = Z - Zlow;
        Zm2 = Z - Zhigh;
        dR = (Rout - Rin) / Nlayers;
        dH = dR / 2;
        dE = dH;
        for tt = 1:Nlayers
            Rs = Rin - dH + dR * tt;
            if abs(R - Rs) >= dE
                % extern
                Rp = Rs + R;
                Rm = Rs - R;
                [Br1,Bz1] = Sol_Rekenb(Zm1,Rm,Rp);
                [Br2,Bz2] = Sol_Rekenb(Zm2,Rm,Rp);
                Brd = Brd + (Br2 - Br1) * Factor * Rs;
                Bzd = Bzd + (Bz1 - Bz2) * Factor;
            else
                % intern
                % RIntern = true;
                Rp = Rs + Rs - dE;
                Rm = dE;
                Weeg = (Rs - R + dE) / dE / 2;
                [Br1,Bz1] = Sol_Rekenb(Zm1,Rm,Rp);
                [Br2,Bz2] = Sol_Rekenb(Zm2,Rm,Rp);
                Brd = Brd + (Br2 - Br1) * Weeg * Factor * Rs;
                Bzd = Bzd + (Bz1 - Bz2) * Weeg * Factor;
                Weeg = 1 - Weeg;
                Rp = Rs + Rs + dE;
                Rm = -dE;
                [Br1,Bz1] = Sol_Rekenb(Zm1,Rm,Rp);
                [Br2,Bz2] = Sol_Rekenb(Zm2,Rm,Rp);
                Brd = Brd + (Br2 - Br1) * Weeg * Factor * Rs;
                Bzd = Bzd + (Bz1 - Bz2) * Weeg * Factor;
            end
        end
    end
end

Brt = Brd * pi * 1e-7;
Bzt = Bzd * pi * 1e-7;

end

%% Additional functions
function [Brh, Bzh] = Sol_Rekenb(Zmh, Rm, Rp)
Tmax = 50;
Hulp = sqrt(Rp * Rp + Zmh * Zmh);
Kc = sqrt(Rm * Rm + Zmh * Zmh) / Hulp;
if Kc < eps; Kc = eps; end
Mj = 1;
Nj = Kc;
Aj = -1;
Bj = 1;
Pj = Rm / Rp;
Cj = Pj + 1;
if abs(Rm / Rp) < eps; Pj = 1; end
Dj = Cj * sign(Pj);
Pj = abs(Pj);
Tel = 1;
% this while loop is slow :(
while abs(Mj - Nj) > (eps * Mj)
    if (Tel > Tmax)
        error('Solution is not converging.');
    end
    Xmn = Mj * Nj;
    Xmp = Xmn / Pj;
    D0 = Dj;
    Dj = 2 * (Dj + Xmp * Cj);
    Cj = Cj + D0 / Pj;
    B0 = Bj;
    Bj = 2 * (Bj + Nj * Aj);
    Aj = Aj + B0 / Mj;
    Pj = Pj + Xmp;
    Mj = Mj + Nj;
    Nj = 2 * sqrt(Xmn);
    Tel = Tel + 1;
end
Brh = (Aj + Bj / Mj) / (Mj * Hulp);
Bzh = (Cj + Dj / Mj) / ((Mj + Pj) * Hulp) * Zmh;
end