// Description:
// Soleno rewrite to MATLAB mex file by Jeroen van Nugteren
// Uses microsofts paralel paterns library to attain maximum performance on multicore systems
// Requires VC-redistributable 2010 to be installed

// Soleno was originally developed at Twente University by Gert Mulder, and upgraded in 1999 by Erik Krooshoop

// This function calculates the field at (R,Z) using:
// [Br,Bz] = soleno(R,Z,Rin,Rout,Zmin,Zmax,I,Nlayers)

// Rin,Rout,Zmin,Zmax,I and Nlayers define the coils and must be equal in size
// R and Z define the position and must be equal in size as well
// output Br and Bz is respectively the field in the radial and axial direction

/* libraries */
#include <mex.h>
#include <cmath>

#ifdef _WIN32 || _WIN64
#include <ppl.h>
#include <windows.h> 
using namespace Concurrency;
#endif

using namespace std;

/* define constants */
const double Pi = 3.1415926535897931;
const double Eps = 1e-12; // calculation accuracy
const int Tmax = 50;

/* Sign Function */
int sign(double a){
    int b;
    if(a>0)b=1;
    if(a<0)b=-1;
    if(a==0)b=0;
    return b;
}

/* calculation function */
void Sol_Rekenb(double *Brh, double *Bzh, double Zmh, double Rm, double Rp){
    double Hulp = sqrt(Rp * Rp + Zmh * Zmh);
    double Kc = sqrt(Rm * Rm + Zmh * Zmh) / Hulp;
    if(Kc < Eps)Kc = Eps;
    double Mj = 1;
    double Nj = Kc;
    double Aj = -1;
    double Bj = 1;
    double Pj = Rm / Rp;
    double Cj = Pj + 1;
    if(abs(Rm / Rp) < Eps)Pj = 1;
    double Dj = Cj * sign(Pj);
    Pj = abs(Pj);
    int Tel = 1;
    while(abs(Mj - Nj) > (Eps * Mj)){
        if (Tel > Tmax)mexErrMsgTxt("Solution does not converge");
        double Xmn = Mj * Nj;
        double Xmp = Xmn / Pj;
        double D0 = Dj;
        Dj = 2 * (Dj + Xmp * Cj);
        Cj = Cj + D0 / Pj;
        double B0 = Bj;
        Bj = 2 * (Bj + Nj * Aj);
        Aj = Aj + B0 / Mj;
        Pj = Pj + Xmp;
        Mj = Mj + Nj;
        Nj = 2 * sqrt(Xmn);
        Tel = Tel + 1;
    }
    
    *Brh = (Aj + Bj / Mj) / (Mj * Hulp);
    *Bzh = (Cj + Dj / Mj) / ((Mj + Pj) * Hulp) * Zmh;
}

/* Original Soleno Function (used by improved version) */
void Sol_CalcB(double *Brt, double *Bzt, double R, double Z, double Rin, double Rout, double Zlow, double Zhigh, double It, int Nlayers){
        
    double Brd = 0;
    double Bzd = 0;
    double Br1,Br2,Bz1,Bz2;
    
    if (Rin != Rout && Zlow != Zhigh){
        double Factor = It / (Zhigh - Zlow) / Nlayers;
        if (Factor != 0){
            double Zm1 = Z - Zlow;
            double Zm2 = Z - Zhigh;
            double dR = (Rout - Rin) / Nlayers;
            double dH = dR / 2;
            double dE = dH;
            for (int tt = 1; tt<=Nlayers; tt++){
                double Rs = Rin - dH + dR * tt;
                if (abs(R - Rs) >= dE){
                    // extern
                    double Rp = Rs + R;
                    double Rm = Rs - R;
                    Sol_Rekenb(&Br1,&Bz1,Zm1,Rm,Rp);
                    Sol_Rekenb(&Br2,&Bz2,Zm2,Rm,Rp);
                    Brd = Brd + (Br2 - Br1) * Factor * Rs;
                    Bzd = Bzd + (Bz1 - Bz2) * Factor;
                }else{
                    // intern
                    // RIntern = true;
                    double Rp = Rs + Rs - dE;
                    double Rm = dE;
                    double Weeg = (Rs - R + dE) / dE / 2;
                    Sol_Rekenb(&Br1,&Bz1,Zm1,Rm,Rp);
                    Sol_Rekenb(&Br2,&Bz2,Zm2,Rm,Rp);
                    Brd = Brd + (Br2 - Br1) * Weeg * Factor * Rs;
                    Bzd = Bzd + (Bz1 - Bz2) * Weeg * Factor;
                    Weeg = 1 - Weeg;
                    Rp = Rs + Rs + dE;
                    Rm = -dE;
                    Sol_Rekenb(&Br1,&Bz1,Zm1,Rm,Rp);
                    Sol_Rekenb(&Br2,&Bz2,Zm2,Rm,Rp);
                    Brd = Brd + (Br2 - Br1) * Weeg * Factor * Rs;
                    Bzd = Bzd + (Bz1 - Bz2) * Weeg * Factor;
                }
            }
        }
    }
    
    *Brt = Brd * Pi * 1e-7;
    *Bzt = Bzd * Pi * 1e-7;
    
}

/* Improved version of Soleno */
/* better field in the radial direction */
void Sol_CalcB_new(double *Brt, double *Bzt, double R, double Z, double Rin, double Rout, double Zlow, double Zhigh, double I, int Nlayers){
    // set layers if not set remotely 5 is default value
    if(Nlayers < 1)Nlayers = 5;
    if (R >= Rin && R <= Rout){
        // internal
        double A = (R - Rin) / (Rout - Rin);
        int NL1 = int(Nlayers * A) + 1;
        int NL2 = int(Nlayers * (1 - A)) + 1;
        
        // deel1
        Sol_CalcB(Brt, Bzt, R, Z, Rin, R, Zlow, Zhigh, I * A, NL1);
        double Br2 = *Brt;
        double Bz2 = *Bzt;
        Sol_CalcB(Brt, Bzt, R, Z, Rin, R, Zlow, Zhigh, I * A, NL1 * 2);
        double BZtemp = *Bzt + (*Bzt - Bz2) / 3; //Extrapolatie naar 1/Nl/Nl=0
        double BRtemp = *Brt + (*Brt - Br2) / 3;
        
        // deel2
        Sol_CalcB(Brt, Bzt, R, Z, R, Rout, Zlow, Zhigh, I * (1 - A), NL2);
        Bz2 = *Bzt;
        Br2 = *Brt;
        Sol_CalcB(Brt, Bzt, R, Z, R, Rout, Zlow, Zhigh, I * (1 - A), NL2 * 2);
        
        *Bzt = BZtemp + *Bzt + (*Bzt - Bz2) / 3; //Extrapolatie naar 1/Nl/Nl=0
        *Brt = BRtemp + *Brt + (*Brt - Br2) / 3;
    }else{
        // external
        Sol_CalcB(Brt, Bzt, R, Z, Rin, Rout, Zlow, Zhigh, I, Nlayers);
        double Bz2 = *Bzt;
        double Br2 = *Brt;
        Sol_CalcB(Brt, Bzt, R, Z, Rin, Rout, Zlow, Zhigh, I, Nlayers * 2);
        *Bzt = *Bzt + (*Bzt - Bz2) / 3; //Extrapolatie naar 1/Nl/Nl=0
        *Brt = *Brt + (*Brt - Br2) / 3;
    }
    
    
}

/* --- MATLAB Gateway Function --- */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    
    /* check number of inputs and outputs */
    if(nrhs!=8)mexErrMsgTxt("8 inputs required");
    if(nlhs!=2)mexErrMsgTxt("2 outputs required");
    
    /* check length of inputs */
    int RN = (int)mxGetN(prhs[0]);
    int RM = (int)mxGetM(prhs[0]);
    int ZN = (int)mxGetN(prhs[1]);
    int ZM = (int)mxGetM(prhs[1]);
    int RinN = (int)mxGetN(prhs[2]);
    int RinM = (int)mxGetM(prhs[2]);
    int RoutN = (int)mxGetN(prhs[3]);
    int RoutM = (int)mxGetM(prhs[3]);
    int ZlowN = (int)mxGetN(prhs[4]);
    int ZlowM = (int)mxGetM(prhs[4]);
    int ZhighN = (int)mxGetN(prhs[5]);
    int ZhighM = (int)mxGetM(prhs[5]);
    int curN = (int)mxGetN(prhs[6]);
    int curM = (int)mxGetM(prhs[6]);
    int nlayersN = (int)mxGetN(prhs[7]);
    int nlayersM = (int)mxGetM(prhs[7]);
    
    if(RN!=ZN || RM!=ZM)mexErrMsgTxt("R and Z must be equal in size");
    if(RinN!=RoutN || RinN!=ZlowN || RinN!=ZhighN || RinN!=curN || RinN!=nlayersN)
        mexErrMsgTxt("Rin, Rout, Zlow, Zhigh and I must be equal in size");
    if(RinM!=RoutM || RinM!=ZlowM || RinM!=ZhighM || RinM!=curM || RinM!=nlayersM)
        mexErrMsgTxt("Rin, Rout, Zlow, Zhigh and I must be equal in size");
    
    /* get function input */
    double *R = (double*)mxGetPr(prhs[0]);
    double *Z = (double*)mxGetPr(prhs[1]);
    double *Rin = (double*)mxGetPr(prhs[2]);
    double *Rout = (double*)mxGetPr(prhs[3]);
    double *Zlow = (double*)mxGetPr(prhs[4]);
    double *Zhigh = (double*)mxGetPr(prhs[5]);
    double *I = (double*)mxGetPr(prhs[6]);
    double *Nlayers = (double*)mxGetPr(prhs[7]);
    
    /* allocate memory for output arrays */
    plhs[0] = mxCreateDoubleMatrix(RM, RN, mxREAL);
    plhs[1] = mxCreateDoubleMatrix(RM, RN, mxREAL);
    
    /* get function output pointers */
    double *Brt = (double*)mxGetPr(plhs[0]);
    double *Bzt = (double*)mxGetPr(plhs[1]);
    
    #ifdef _WIN32 || _WIN64
    /* lower priorty prevents system from freezing during calculation*/
    SetPriorityClass(GetCurrentProcess(),BELOW_NORMAL_PRIORITY_CLASS);
    #endif
    
    /* walk over target points */
    
    #ifdef _WIN32 || _WIN64
    parallel_for(0,RN*RM,[&](int i){
    #else
    int chunk = 10, i;
    #pragma omp parallel shared(R,Z,Rin,Rout,Zlow,Zhigh,I,Nlayers) private(i)
    {
    #pragma omp for schedule(dynamic, chunk)
    for(i=0;i<RN*RM;i++){
    #endif
        for(int j=0;j<RinN*RinM;j++){
            double Br,Bz;
            if (R[i] >= 0){
                Sol_CalcB_new(&Br,&Bz,R[i],Z[i],Rin[j],Rout[j],Zlow[j],Zhigh[j],I[j],(int)Nlayers[j]);
                Brt[i] = Brt[i] + Br; Bzt[i] = Bzt[i] + Bz;
            }else{
                Sol_CalcB_new(&Br,&Bz,-R[i],Z[i],Rin[j],Rout[j],Zlow[j],Zhigh[j],I[j],(int)Nlayers[j]);
                Brt[i] = Brt[i] - Br; Bzt[i] = Bzt[i] + Bz;
            }
        }
    #ifdef _WIN32 || _WIN64
    });
    #else
    }
    }
    #endif
   
    
    #ifdef _WIN32 || _WIN64
    /* set priority back to normal (who knows what will happen if we don't) */
    SetPriorityClass(GetCurrentProcess(),NORMAL_PRIORITY_CLASS);
    #endif
}








