%% target points
R = linspace(-0.4,0.4,1000);
Z = linspace(-0.4,0.4,1000);

[R2,Z2] = meshgrid(R,Z);

%% Definition of magnet
Rin = [0.13,0.13];
Rout = [0.15,0.15];
Zlow = [-0.1,0.05];
Zhigh = [-0.05,0.1];

Nturn = [200,200];
I = [200,200];
Nloop = [5,5];
Itot = Nturn.*I;

%% Calculation
[Br,Bz] = soleno_calcB(R2,Z2,Rin,Rout,Zlow,Zhigh,Itot,Nloop);

%% Inductance matrix
M = soleno_calcM(Rin,Rout,Zlow,Zhigh,Nturn,Nloop);

%% Output
% plot
f = figure('Color','w'); ax = axes('Parent',f); 
hold(ax,'on'); grid(ax,'on'); box(ax,'on');
contour(R2,Z2,sqrt(Br.^2+Bz.^2),30,'LineWidth',1);
colorbar('EastOutside','peer',ax);
axis(ax,'equal');
axis(ax,'tight');

% matrix
disp('Inductance Matrix');
disp(M);

